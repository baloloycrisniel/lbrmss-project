<?php
   
  // Tells the browser to allow code from any origin to access
  header('Access-Control-Allow-Origin: *');
  // Tells browsers whether to expose the response to the frontend JavaScript code when the request's credentials mode (Request.credentials) is include
  header("Access-Control-Allow-Credentials: true");
  // Specifies one or more methods allowed when accessing a resource in response to a preflight request
  header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
  // Used in response to a preflight request which includes the Access-Control-Request-Headers to indicate which HTTP headers can be used during the actual request
  header("Access-Control-Allow-Headers: Content-Type");
 
  require_once('../MysqliDb.php');
 session_start();

 $_SESSION['status']='invalid';
  
  class API
  {
      public function __construct()
      {
       $this->db = new MysqliDB('localhost', 'root', '', 'lbrmss');
        
      }

      public function httpGet($payload)
      {

      }
      public function httpPOST($payload)
      {

        $datas = json_encode($payload);
        $arr = json_decode($datas, true);

       $username = $arr['username'];
       $password = $arr['password'];
        
       $this->db->where ("Username", $username);
       $account = $this->db->getOne("useraccounts_tbl");
       if($account < 1){
        echo json_encode(array("Status" => "Failed" . $this->db->getLastError()));
     }
     else{
      $accountpassword = $account['Password'];
      
         // MATCH THE PLAIN PASSWORD TO HASH PASSWORD
         $verify = password_verify($password, $accountpassword );
        
         //  Print the result depending if they match
         
          if ($verify) { 
           http_response_code(200);
           $_SESSION['status'] = 'valid';
           echo json_encode(array("Status"=> "Success" ,"data"=>$account, "Mehtod"=>"POST" , $_SESSION['status'] ));
         
          } else {
           echo json_encode(array("Status" => "Failed" . $this->db->getLastError()));
          }
   
     }
      
       
    
      }
      public function httpPut($payload)
      {
         
    }
      public function httpDelete($payload)
      {
    
       
        
      }
  }
  

    $request_method = $_SERVER['REQUEST_METHOD'];
    $received_data = json_decode(file_get_contents('php://input'));
   

  $Api = new API;

  if ($request_method == 'GET') {
      $Api->httpGet($_GET);
  }
  if ($request_method == 'POST') {
      $Api->httpPost($received_data);
  }
  if ($request_method == 'PUT') {
      $Api->httpPut($received_data);
  }
  if ($request_method == 'DELETE') {
      $Api->httpDelete($received_data);
  }