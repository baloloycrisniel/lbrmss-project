<?php
   
  // Tells the browser to allow code from any origin to access
  header('Access-Control-Allow-Origin: *');
  // Tells browsers whether to expose the response to the frontend JavaScript code when the request's credentials mode (Request.credentials) is include
  header("Access-Control-Allow-Credentials: true");
  // Specifies one or more methods allowed when accessing a resource in response to a preflight request
  header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
  // Used in response to a preflight request which includes the Access-Control-Request-Headers to indicate which HTTP headers can be used during the actual request
  header("Access-Control-Allow-Headers: Content-Type");
 
  require_once('../MysqliDb.php');
 
  
  class API
  {
      public function __construct()
      {
       $this->db = new MysqliDB('localhost', 'root', '', 'lbrmss');
        
      }

      public function httpGet($payload)
      {
        
    

      }
      public function httpPost($payload)
      {
       
        $datas = json_encode($payload);
        $arr = json_decode($datas, true);
       
        $name = $arr["payload"]["name"];
        $user = $arr["payload"]["username"];
        $role = $arr["payload"]["position"];
        $pass = $arr["payload"]["password"];
        // DECRYPT PASS
        $hashedPassword = password_hash($pass, PASSWORD_DEFAULT);
        echo $hashedPassword;
        $data = Array ("name" => $name,
                       "Username" => $user,
                        "Position" => $role,
                        "Password" => $hashedPassword
                      );
        // INSERT
        $insert_data = $this->db->insert('useraccounts_tbl', $data);
                        
        if($insert_data){
            http_response_code(200);
            echo json_encode("Account Created");

        }
        else{
            echo json_encode(array('msg' => "Failed: " . $db->getLastError()));
        }

    }
  
      
      public function httpPut($payload)
      {
         
    }
      public function httpDelete($payload)
      {
    
       
        
      }
  }
  

    $request_method = $_SERVER['REQUEST_METHOD'];
    $received_data = json_decode(file_get_contents('php://input'));
   

  $api = new API;

  if ($request_method == 'GET') {
      $api->httpGet($_GET);
  }
  if ($request_method == 'POST') {
      $api->httpPost($received_data);
  }
  if ($request_method == 'PUT') {
      $api->httpPut($received_data);
  }
  if ($request_method == 'DELETE') {
      $api->httpDelete($received_data);
  }