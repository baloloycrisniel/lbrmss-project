// Note: Composable file must consist only a CRUD Functionality
import { ref, readonly } from "vue";

import { api } from "../boot/axios";
let Positiondata = ref([]);
let Namedata = ref([]);
let status = ref();
/**
 * This function accepts parameters of an array then
 * set the passed array to unitWork data.
  @param {} object
 */
const LoginPayload = (payload) => {
  return new Promise((resolve, reject) => {
    api
      .post("Login.php", payload)
      .then((response) => {
        // name status position data
        status.value = response.data.Status;
        if (status.value === "Success") {
          Positiondata.value = response.data.data.Position;
          status.value = response.data.Status;
          Namedata.value = response.data.data.Name;
        } else {
          status.value = response.data.Status;
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export { LoginPayload, status, Positiondata, Namedata };
