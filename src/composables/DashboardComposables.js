// Note: Composable file must consist only a CRUD Functionality
import { ref, readonly } from "vue";
import { api } from "../boot/axios";
let data = ref();
let countPending = ref();
let tbData = ref([]);
let updatedDATA = ref();
let showMarriageData = ref([]);

// SECRETARY DASHBOARD GET card DATA
const Dashboard = (payload) => {
  return new Promise((resolve, reject) => {
    api
      .get("Dashboard.php")
      .then((response) => {
        // name status position data
        data.value = response.data.data;
        countPending.value = data.value.length;
      })
      .catch((error) => {
        reject(error);
      });
  });
};
//  Fetch all DATA for tables
const TABLEDATA = (payload) => {
  return new Promise((resolve, reject) => {
    api
      .get("MarriageApi.php")
      .then((response) => {
        tbData.value = response.data.data;
        console.log(tbData.value);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

//  ADD RECORDS
const SendFormData = (payload) => {
  console.log(payload);
  if ("Groom_First_Name" in payload) {
    return new Promise((resolve, reject) => {
      api
        .post("MarriageApi.php", { MarriageData: payload })
        .then((response) => {
          tbData.value = response.data;
          console.log(tbData.value);
        })
        .catch((error) => {
          reject(error);
        });
    });
  } else {
    console.log("error");
  }
};
// View Selected Data
// Fetch Specific Data from  TABLE
const ViewFormData = (payload) => {
  return new Promise((resolve, reject) => {
    api
      .post("MarriageApi.php", { ViewMarriageData: payload })
      .then((response) => {
        showMarriageData.value = response.data.data;
        console.log(showMarriageData.value);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

// Update Form Data
const UpdateFormData = (payload) => {
  console.log(payload);
  if ("Groom_First_Name" in payload) {
    console.log("predse");
    return new Promise((resolve, reject) => {
      api
        .put("MarriageApi.php", { UpdatedMarriageData: payload })
        .then((response) => {
          // showMarriageData.value = response.data.data;
          // console.log(showMarriageData.value);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
};

export {
  Dashboard,
  data,
  countPending,
  SendFormData,
  UpdateFormData,
  TABLEDATA,
  tbData,
  ViewFormData,
  showMarriageData,
};
