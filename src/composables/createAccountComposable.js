// Note: Composable file must consist only a CRUD Functionality
import { ref, readonly } from "vue";

import { api } from "../boot/axios";
let data = ref([]);
let status = ref();
/**
 * This function accepts parameters of an array then
 * set the passed array to unitWork data.
  @param {} object
 */
const Send_AccountPayload = (payload) => {
  return new Promise((resolve, reject) => {
    api
      .post("createUser.php", {
        payload,
      })
      .then((response) => {
        data.value = response.data;
        status.value = response.data.Status;
      })
      .catch((error) => {
        reject(error);
      });
  });
};

// infinite Scroll
const inifiniteScrollRequest = (payload) => {};

const fetchEmployee = (payload) => {
  // console.log(payload);
};

/** parameters of an object then
 * add the passed object
 * This function accepts to unitWork data.
  @param {} object
 */

const AddEmployeeData = (payload) => {};
const ExcelBulkUpload = (payload) => {};

/**
 * This function accepts parameters of an object then
 * updates the unitWork specific data based on the id passed in the object.
  @param {} object
 */
// rename to getemployee info
const showEmployeeInfo = (payload) => {};

const UpdateForm = (payload) => {};

/**
 * This function accepts parameters of an array like this [1,2,3,4] then
 * delete the data in the unitWork based on this parameter.
  @param {} array
 */

const DeleteUnitWork = (payload) => {};
const DeleteAll = (payload) => {};

/**
 * Export unitWorkList as readonly (real time copy of unitWork)
 */
export { Send_AccountPayload, status };
