import { defineComponent, ref } from "vue";
import {
  Send_AccountPayload,
  status,
} from "../../composables/createAccountComposable";
import { useQuasar } from "quasar";
export default defineComponent({
  setup() {
    const $q = useQuasar();
    // create_account Details
    let AccountDataPayload = ref({
      name: null,
      username: null,
      position: null,
      password: null,
      confirm_password: null,
    });

    //  submit function
    let warning = ref(null);
    let submit_Account_data = (payload) => {
      if (payload.password != payload.confirm_password) {
        warning.value = "Password doesn't match, Try again";
        console.log(warning.value);
      } else {
        Send_AccountPayload(payload);
        warning.value = "";

        if ((status.value = "Success")) {
          $q.notify({
            message: "New Account has been Created",
            color: "green-5",
          });
        } else {
          $q.notify({
            message: "Please try again",
            color: "danger",
          });
        }
      }
    };
    //  submit function END

    return {
      AccountDataPayload,
      submit_Account_data,
      warning,
      password: ref(""),
      isPwd: ref(true),
      dense: ref(true),
    };
  },
});
