import { defineComponent, ref, onMounted } from "vue";
import { Dashboard, data } from "../../composables/DashboardComposables";
import { status, Positiondata, Namedata } from "../../composables/LoginComp";
import { useQuasar } from "quasar";
import { useRouter, useRoute } from "vue-router";
export default defineComponent({
  setup() {
    const $q = useQuasar();
    let getPending = ref();

    return {
      onload,
      getPending,
      data,
    };
  },
  beforeMount() {
    Dashboard();
  },
});
