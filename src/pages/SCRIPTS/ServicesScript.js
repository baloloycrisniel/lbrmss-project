import { defineComponent, ref, onMounted } from "vue";
import {
  Dashboard,
  data,
  TABLEDATA,
  tbData,
  ViewFormData,
  showMarriageData,
} from "../../composables/DashboardComposables";
import { status, Positiondata, Namedata } from "../../composables/LoginComp";
import { useQuasar } from "quasar";
import { useRouter, useRoute } from "vue-router";
const stringOptions = ["Google", "Facebook", "Twitter", "Apple", "Oracle"];
export default defineComponent({
  setup() {
    const $q = useQuasar();
    const router = useRouter();
    // Table Columns

    const columns = [
      {
        name: "index",
        label: "No.",
        field: "index",
      },
      {
        name: "Date",
        required: true,
        label: "Date",
        align: "center",
        field: (row) => row.name,
        format: (val) => `${val}`,
        sortable: true,
      },
      {
        name: "Name",
        align: "center",
        label: "Name of Bride and Groom",
        field: "Name",
        sortable: true,
      },
      {
        name: "Priest",
        label: "Assigned Priest",
        field: "Priest",
        align: "center",
        sortable: true,
      },
      { name: "Address", label: "Address", field: "Address", align: "center" },
      { name: "Action", label: "Action", field: "Action", align: "center" },
    ];
    (row) => {
      // Generate a unique identifier for each row
      return `row-${this.tableData.indexOf(row) + 1}`;
    };
    // Search Bar
    const options = ref(stringOptions);

    let model = ref(null);
    let update;

    let filterFn = (val, update, abort) => {
      update(() => {
        const needle = val.toLowerCase();
        options.value = stringOptions.filter(
          (v) => v.toLowerCase().indexOf(needle) > -1
        );
      });
    };
    // View Marriage RowData
    let ViewMarriageRow = (payload) => {
      ViewFormData(payload);
      router.push(`/SecretaryDashboard/Marriage/MarriageShowData/${payload}`);
    };
    // Edit Marriage Row Data
    let editMarriageData = (payload) => {};
    // print
    let PrintMarriageData = (payload) => {
      ViewFormData(payload);
      router.push(`/SecretaryDashboard/Marriage/MarriagePrint/${payload}`);
    };

    return {
      columns,
      options,
      filterFn,
      model,
      tbData,
      editMarriageData,
      ViewMarriageRow,
      showMarriageData,
      PrintMarriageData,
    };
  },
  beforeMount() {
    TABLEDATA();
  },
});
