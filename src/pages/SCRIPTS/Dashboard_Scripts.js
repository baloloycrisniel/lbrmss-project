import { defineComponent, ref, onMounted } from "vue";
import { status, Positiondata, Namedata } from "../../composables/LoginComp";
import { useQuasar } from "quasar";
import { useRouter, useRoute } from "vue-router";
export default defineComponent({
  setup() {
    const $q = useQuasar();
    const miniState = ref(false);

    let logout = () => {
      $q.dialog({
        title: "",
        message: "Are you sure you want to logout?",
        cancel: true,
        persistent: true,
      })
        .onOk(() => {
          console.log(">>>> OK");
        })
        .onOk(() => {
          // console.log('>>>> second OK catcher')
        })
        .onCancel(() => {
          // console.log('>>>> Cancel')
        })
        .onDismiss(() => {
          // console.log('I am triggered on both OK and Cancel')
        });
    };

    return {
      logout,
      Positiondata,
      Namedata,

      drawer: ref(true),
      miniState,

      drawerClick(e) {
        // if in "mini" state and user
        // click on drawer, we switch it to "normal" mode
        if (miniState.value) {
          miniState.value = false;

          // notice we have registered an event with capture flag;
          // we need to stop further propagation as this click is
          // intended for switching drawer to "normal" mode only
          e.stopPropagation();
        }
      },
    };
  },
});
