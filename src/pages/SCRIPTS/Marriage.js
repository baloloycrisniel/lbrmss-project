import { defineComponent, ref, onMounted } from "vue";
import {
  SendFormData,
  showData,
  ViewFormData,
} from "../../composables/DashboardComposables.js";

import { status, Positiondata, Namedata } from "../../composables/LoginComp";
import { useQuasar } from "quasar";

export default defineComponent({
  setup() {
    const $q = useQuasar();

    let MarriageData = ref({
      MID: null,
      Groom_First_Name: null,
      Groom_Middle_Name: null,
      Groom_Last_Name: null,
      Groom_Suffix: null,
      Groom_Status: null,
      Groom_BirthDate: null,
      Groom_Address: null,
      Grooms_Age: null,
      Bride_First_Name: null,
      Bride_Middle_Name: null,
      Bride_Last_Name: null,
      Bride_BirthDate: null,
      Bride_Status: null,
      Bride_Age: null,
      Bride_Address: null,
      Groom_Father: null,
      Groom_Mother: null,
      Bride_Suffix: null,
      Bride_Father: null,
      Bride_Mother: null,
      Groom_Testium_2: null,
      Bride_Testium_1: null,
      Bride_Testium_2: null,
      Groom_T1_Address: null,
      Groom_T2_Address: null,
      Bride_T1_Address: null,
      Bride_T2_Address: null,
      Observanda: null,
      Date_of_Marriage: null,
      Stipendium: null,
      Assigned_Priest: null,

      Created_at: new Date().toLocaleString(),
      // Created_by: Namedata.value,
      Modified_at: new Date().toLocaleString(),
      Modified_by: Namedata.value,
    });
    const nameRef = ref(null);
    let onSubmit = (payload) => {
      console.log(payload);
      SendFormData(payload);

      $q.notify({
        color: "green-4",
        textColor: "white",
        icon: "cloud_done",
        message: "Submitted",
      });
    };

    let onReset = (payload) => {
      for (let key in payload) {
        payload[key] = null; // or null
      }
    };
    // MArrriage Add Record End
    let ViewMarriageRow = (payload) => {
      console.log(payload);
    };
    // Edit Marriage Row Data
    let editMarriageData = (payload) => {
      ViewFormData(payload);
      router.push(`/Marriage/MarriageShowData/${payload}`);
    };
    // file picker
    const fileInput = ref(null);

    const pickFile = () => {
      fileInput.value.click();
    };
    let imageUrl = [];
    let images = ref([]);
    const handleFileUpload = (event) => {
      // const file = event.target.files[0];

      // imageUrl.value = URL.createObjectURL(file);
      const files = event.target.files;
      for (let i = 0; i < files.length; i++) {
        const file = files[i];
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          images.value.push(reader.result);
        };
      }
      // console.log(tmpName.value); // do something with the file
    };
    return {
      MarriageData,
      onSubmit,
      onReset,
      nameRef,
      ViewMarriageRow,
      editMarriageData,
      fileInput,
      pickFile,
      handleFileUpload,
      imageUrl,
      images,
    };
  },
});
