import { defineComponent, ref } from "vue";
import {
  LoginPayload,
  status,
  Positiondata,
} from "../../composables/LoginComp";
import { useQuasar } from "quasar";
import { useRouter, useRoute } from "vue-router";

export default defineComponent({
  setup() {
    const $q = useQuasar();
    const inputRef = ref(null);
    const router = useRouter();
    let loginData = ref({
      username: null,
      password: null,
    });
    let login = (payload) => {
      // validate
      if (payload.username == null || payload.password == null) {
        $q.notify({
          message: "Please Fill out the form",
          color: "red-9",
        });
      } else {
        // send payload
        LoginPayload(payload);
        // condition
        if (status.value === "Failed") {
          $q.notify({
            message: "Username or Password is Incorrect!",
            color: "red-9",
          });
        }
        if (status.value === "Success") {
          // add function that will navigate to another dashboard such as cashier
          router.push({
            path: "/SecretaryDashboard",
          });
        }
      }
    };
    return {
      inputRef,
      password: ref(""),
      isPwd: ref(true),
      dense: ref(true),
      login,
      loginData,
    };
  },
});
