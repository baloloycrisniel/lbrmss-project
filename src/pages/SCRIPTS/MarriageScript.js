import { defineComponent, ref, onMounted } from "vue";
import {
  SendFormData,
  showData,
  ViewFormData,
  showMarriageData,
  UpdateFormData,
} from "../../composables/DashboardComposables.js";

import { status, Positiondata, Namedata } from "../../composables/LoginComp";
import { useQuasar } from "quasar";

export default defineComponent({
  setup() {
    const $q = useQuasar();
    let isHidden = ref(true);
    let Marriage = ref({
      MID: null,
      Groom_First_Name: null,
      Groom_Middle_Name: null,
      Groom_Last_Name: null,
      Groom_Suffix: null,
      Groom_Status: null,
      Groom_BirthDate: null,
      Groom_Address: null,
      Grooms_Age: null,
      Bride_First_Name: null,
      Bride_Middle_Name: null,
      Bride_Last_Name: null,
      Bride_BirthDate: null,
      Bride_Status: null,
      Bride_Age: null,
      Bride_Address: null,
      Groom_Father: null,
      Groom_Mother: null,
      Bride_Suffix: null,
      Bride_Father: null,
      Bride_Mother: null,
      Groom_Testium_2: null,
      Bride_Testium_1: null,
      Bride_Testium_2: null,
      Groom_T1_Address: null,
      Groom_T2_Address: null,
      Bride_T1_Address: null,
      Bride_T2_Address: null,
      Observanda: null,
      Date_of_Marriage: null,
      Stipendium: null,
      Assigned_Priest: null,

      Created_at: new Date().toLocaleString(),
      // Created_by: Namedata.value,
      Modified_at: new Date().toLocaleString(),
      Modified_by: Namedata.value,
    });
    let isDisable = ref(true);
    // MArrriage Add Record End

    let ViewMarriageRow = (payload) => {
      ViewFormData(payload);
      router.push(`/Marriage/MarriageShowData/${payload}`);
    };
    // Edit Marriage Row Data
    let enableDisable = () => {
      isDisable.value = !isDisable.value;
      isHidden.value = !isHidden.value;
    };
    let editMarriageData = (payload) => {
      UpdateFormData(payload);
    };
    let print = () => {
      window.print();
    };
    return {
      ViewMarriageRow,
      editMarriageData,
      showMarriageData,
      isDisable,
      enableDisable,
      Marriage,
      isHidden,
      print,
    };
  },
});
