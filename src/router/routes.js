import SecretaryPage from "../pages/IndexPage";
import HomePage from "../pages/NavPages/HomePage.vue";
import EventPage from "../pages/NavPages/EventsPage.vue";
import BaptismPage from "../pages/NavPages/BaptismPage.vue";
import MarriagePage from "../pages/NavPages/clientRecords/MarriagePage.vue";
import AddMarriagePage from "../pages/NavPages/clientRecords//MarriageAddData.vue";
import Records_Marriage from "../pages/NavPages/clientRecords/MarriageDefaultPage.vue";
import ShowMarriageData from "../pages/NavPages/clientRecords/MarriageShowData.vue";
import MarriagePrint from "../pages/NavPages/clientRecords/MarriagePrint.vue";
import FinancePage from "../pages/NavPages/FinancePage.vue";
const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "",
        component: () => import("pages/LoginPage.vue"),
      },
      {
        path: "/CreateAccount",
        component: () => import("pages/CreateUserPage.vue"),
      },
    ],
  },
  {
    path: "/SecretaryDashboard",
    name: "SecretaryPage",
    component: SecretaryPage,
    children: [
      {
        path: "",
        name: HomePage,
        component: HomePage,
      },
      {
        path: "/SecretaryDashboard/Events",
        name: EventPage,
        component: EventPage,
      },
      {
        path: "/SecretaryDashboard/Baptism",
        name: BaptismPage,
        component: BaptismPage,
      },
      {
        path: "/SecretaryDashboard/Marriage",
        name: MarriagePage,
        component: MarriagePage,
        children: [
          {
            path: "",
            name: Records_Marriage,
            component: Records_Marriage,
          },
          {
            path: "/SecretaryDashboard/Marriage/MarriageAddData",
            name: AddMarriagePage,
            component: AddMarriagePage,
          },
          {
            path: "/SecretaryDashboard/Marriage/MarriageShowData/:id",
            name: ShowMarriageData,
            component: ShowMarriageData,
          },
        ],
      },
      {
        path: "/Finance",
        name: FinancePage,
        component: FinancePage,
      },
    ],
  },
  {
    path: "/SecretaryDashboard/Marriage/MarriagePrint/:id",
    name: MarriagePrint,
    component: MarriagePrint,
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
